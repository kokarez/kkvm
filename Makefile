OBJS=src/main.o src/kvm_helper.c
CC=clang
TARGET=kkvm

all: $(TARGET)
	make -C vm
	
$(TARGET):	$(OBJS)
	$(CC) $^ -o $(TARGET) -g

%.o: %.c
	$(CC) -c $< -o $@ -g

clean:
	rm -rf */*.o $(TARGET)
	make -C vm clean
