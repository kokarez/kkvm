#include "kvm_helper.h"

static kkvm_kvm   kvm;
static kkvm_vm    vm;

void kkvm_exit(char *err_str, int err_code)
{
  printf("%s", err_str);
  exit(err_code);
}

void kkvm_init(void)
{
  kvm.fd = open("/dev/kvm", O_RDWR);
  if (kvm.fd == -1)
    kkvm_exit("Failed open \"/dev/kvm\"\n", 1);

  kvm.api_version = ioctl(kvm.fd, KVM_GET_API_VERSION, 0);
  if (kvm.api_version != 12)
    kkvm_exit("Bad version\n", kvm.api_version);

  kvm.run_size = ioctl(kvm.fd, KVM_GET_VCPU_MMAP_SIZE, 0);
  if (kvm.run_size == -1)
    kkvm_exit("Failed to get run size\n", 1);

  vm.cpu_count = 0;
  vm.region_count = 0;

  vm.fd = ioctl(kvm.fd, KVM_CREATE_VM, 0);
  if (vm.fd == -1)
    kkvm_exit("Failed to create VM\n", 1);

  kvm.vm = &vm;
}

void kkvm_add_cpu(void)
{
  kkvm_cpu *cpu;

  cpu = &vm.cpus[vm.cpu_count];
  cpu->fd = ioctl(vm.fd, KVM_CREATE_VCPU, 1);
  if (cpu->fd == -1)
  {
    printf("WARN: failed to add cpu [%d]\n", vm.cpu_count);
    return;
  }
  cpu->kvm_run = mmap(NULL,
                      kvm.run_size,
                      PROT_READ | PROT_WRITE,
                      MAP_SHARED,
                      cpu->fd,
                      0);
  vm.cpu_count++;
}


void kkvm_add_region(__u64 begin, __u64 size)
{
  struct kvm_userspace_memory_region region;

  void *map = mmap(NULL,
                   size,
                   PROT_READ | PROT_WRITE,
                   MAP_PRIVATE | MAP_ANONYMOUS,
                   -1,
                   0);

  if (map == MAP_FAILED)
  {
    printf("WARN: Failed mmap\n");
    return;
  }

  region.slot = vm.region_count;
  region.flags = 0;
  region.guest_phys_addr = begin;
  region.memory_size = size;
  region.userspace_addr = (__u64)map;

  memcpy(vm.regions + vm.region_count, &region, sizeof(region));

  if (ioctl(vm.fd, KVM_SET_USER_MEMORY_REGION, &region) == -1)
  {
    printf("WARN: Failed to set new region: 0x%llx with %lld bytes\n", begin, size);
    return;
  }
  vm.region_count++;
}

void kkvm_load(const char *filepath)
{
  int                 fd = open(filepath, O_RDONLY);
  struct stat         stat;
  unsigned long long  entry;

  if (fd == -1)
    kkvm_exit("Failed to open file\n", 1);

  fstat(fd, &stat);

  void *file = mmap(NULL,
                    stat.st_size,
                    PROT_READ,
                    MAP_PRIVATE,
                    fd,
                    0);

  if (file == MAP_FAILED)
    kkvm_exit("Failed to map file\n", 1);

  /* TODO: change this */
  entry = 0x7c00;

  memcpy((char*)vm.regions[0].userspace_addr + entry, file, stat.st_size);
}

void kkvm_prerun(void)
{
  if (ioctl(kvm.fd, KVM_CHECK_EXTENSION, KVM_CAP_SET_TSS_ADDR))
  {
    /* TODO: find space for tss */

    if(ioctl(vm.fd, KVM_SET_TSS_ADDR, 0x1000000) < 0)
    {
      kkvm_exit("Can't add needed TSS", 1);
    }
  }
}

int  kkvm_run(int cpu_fd)
{
  return ioctl(cpu_fd, KVM_RUN, 0);
}


kkvm_kvm *kkvm_get_kvm(void)
{
  return &kvm;
}

/*
 *  Error to string
 */

char *kvm_exit_str[] = {
  "KVM_EXIT_UNKNOWN",
  "KVM_EXIT_EXCEPTION",
  "KVM_EXIT_IO",
  "KVM_EXIT_HYPERCALL",
  "KVM_EXIT_DEBUG",
  "KVM_EXIT_HLT",
  "KVM_EXIT_MMIO",
  "KVM_EXIT_IRQ_WINDOW_OPEN",
  "KVM_EXIT_SHUTDOWN",
  "KVM_EXIT_FAIL_ENTRY",
  "KVM_EXIT_INTR",
  "KVM_EXIT_SET_TPR",
  "KVM_EXIT_TPR_ACCESS",
  "KVM_EXIT_S390_SIEIC",
  "KVM_EXIT_S390_RESET",
  "KVM_EXIT_DCR",
  "KVM_EXIT_NMI",
  "KVM_EXIT_INTERNAL_ERROR",
  "KVM_EXIT_OSI",
  "KVM_EXIT_PAPR_HCALL  ",
  "KVM_EXIT_S390_UCONTROL ",
  "KVM_EXIT_WATCHDOG",
  "KVM_EXIT_S390_TSCH",
  "KVM_EXIT_EPR",
  "KVM_EXIT_IO_IN",
  "KVM_EXIT_IO_OUT",
  "KVM_YOU_SUCKS"
};
