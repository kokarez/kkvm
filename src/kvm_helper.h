#ifndef KVM_HELPER_H
# define KVM_HELPER_H

# include <sys/types.h>
# include <sys/stat.h>
# include <sys/mman.h>
# include <sys/ioctl.h>
# include <fcntl.h>
# include <stdio.h>
# include <string.h>
# include <stdlib.h>
# include <linux/kvm.h>

# define MAX_CPU      16
# define MAX_REGION   16

typedef struct {
  int             fd;
  struct kvm_run  *kvm_run;
} kkvm_cpu;

typedef struct kvm_userspace_memory_region kkvm_region;

typedef struct {
  int             fd;
  int             cpu_count;
  kkvm_cpu        cpus[MAX_CPU];
  int             region_count;    
  kkvm_region     regions[MAX_REGION];
  int             tss;
} kkvm_vm;

typedef struct {
  int             fd;
  int             api_version;
  int             run_size;
  kkvm_vm         *vm;
} kkvm_kvm;

void kkvm_exit(char *err_str, int err_code);
void kkvm_init(void);
void kkvm_add_cpu(void);
void kkvm_add_region(__u64 begin, __u64 size);
void kkvm_load(const char *filepath);
void kkvm_prerun(void);
int  kkvm_run(int cpu_fd);

kkvm_kvm *kkvm_get_kvm(void);

#endif /* KVM_HELPER_H */
