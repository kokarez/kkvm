/* Mine */

#include "kvm_helper.h"

/* export */

extern char *kvm_exit_str[];


int main(int argc , char **argv)
{
  int       res;
  kkvm_kvm  *kvm;

  if (argc < 2) {
    printf("Usage: ./kkvm mvRaw\n");
    return 1;
  }

  kkvm_init();
  kkvm_add_cpu();
  kkvm_add_region(0x0, 0x100000);
  kkvm_load(argv[1]);
  kkvm_prerun();
  kvm = kkvm_get_kvm();

  while (1) {
    res = kkvm_run(kvm->vm->cpus[0].fd);
    res = kvm->vm->cpus[0].kvm_run->exit_reason;
    printf("VMEXIT: [%d] %s\n", res, kvm_exit_str[(res > 23 ? 24 : res)]);
  }

  return 0;
}
